from random import choice, shuffle, randint

vowels = ['а', 'о', 'у', 'ы', 'э', 'я', 'ю', 'и', 'е']
consonants = [
    'б', 'в', 'г', 'д', 'з', 'к', 'л', 'м',
    'н', 'п', 'р', 'с', 'т', 'ф', 'х',
]


def get_random_syllable() -> str:
    couple_count = randint(0, 2)
    if couple_count:
        couple = [choice(vowels), choice(consonants)]
        shuffle(couple)
    else:
        couple = choice(choice([vowels, consonants]))
    return ''.join(couple)


def get_random_syllable_2() -> str:
    couple = [choice(vowels), choice(consonants)]
    shuffle(couple)
    return ''.join(couple)


def get_random_name() -> str:
    syllables_count = randint(2, 4)
    return ''.join([get_random_syllable_2() for _ in range(syllables_count)])


if __name__ == '__main__':
    for _ in range(30):
        print(get_random_name().capitalize())
