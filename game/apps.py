from django.apps import AppConfig


class GameConfig(AppConfig):
    name = 'game'
    verbose_name = 'игра'

    def ready(self):
        import game.signals