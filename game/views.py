from typing import List
from django.db.models import query
from django.shortcuts import render, HttpResponse
from django.views.generic import (
    View,
    TemplateView,
    RedirectView,
    DetailView,
    ListView,
    UpdateView,
)
from rest_framework.viewsets import ModelViewSet, ReadOnlyModelViewSet
from game.serializers import (
    GamerSerializer,
    CardTypeSerializer,
    CustGamerSerializer,
    CardSerializer,
    DeckSerializer,
)
from game.models import (
    Gamer,
    CardType,
    Card,
    Deck,
)
from game.forms import (
    CardForm,
)


###############################################################################
##                              VIEWSETS FOR API                             ##
###############################################################################


class GamerViewSet(ReadOnlyModelViewSet):
    serializer_class = GamerSerializer
    queryset = Gamer.objects.all()


class CustGamerViewSet(ModelViewSet):
    serializer_class = CustGamerSerializer
    queryset = Gamer.objects.all()


class CardTypeViewSet(ReadOnlyModelViewSet):
    serializer_class = CardTypeSerializer
    queryset = CardType.objects.filter(is_active=True)


class CardViewSet(ModelViewSet):
    serializer_class = CardSerializer
    queryset = Card.objects.all()


class DeckViewSet(ModelViewSet):
    serializer_class = DeckSerializer
    queryset = Deck.objects.all()



###############################################################################
##                              VIEWS FOR PAGES                              ##
###############################################################################


class HomeView(TemplateView):
    template_name = "home.html"


class GamersListView(ListView):
    model = Gamer
    context_object_name = 'gamers'
    template_name = 'gamers_list.html'
    
    def get_queryset(self):
        return self.model.objects.all()


class GamerDetail(DetailView):
    model = Gamer
    template_name='gamer_detail.html'
    context_object_name = 'gamer'


class DeckDetail(DetailView):
    model = Deck
    template_name='deck_detail.html'
    context_object_name = 'deck'


class CardEdit(UpdateView):
    form_class = CardForm
    model = Card
    template_name = 'card_edit.html'
    context_object_name = 'card'