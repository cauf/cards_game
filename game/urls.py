from django.urls import path
from rest_framework.routers import DefaultRouter
from game.views import (
    GamerViewSet,
    CardTypeViewSet,
    CustGamerViewSet,
    CardViewSet,
    DeckViewSet,
    HomeView,
    GamersListView,
    GamerDetail,
    DeckDetail,
    CardEdit,
)

app_name = 'game'

router = DefaultRouter()

router.register('api/gamer', GamerViewSet)
router.register('api/card_type', CardTypeViewSet)
router.register('api/tst', CustGamerViewSet)
router.register('api/card', CardViewSet)
router.register('api/deck', DeckViewSet)

urlpatterns = [
    path('', HomeView.as_view()),
    path('gamer', GamersListView.as_view()),
    path('gamer/<int:pk>', GamerDetail.as_view(), name='gamer_detail'),
    path('deck/<int:pk>', DeckDetail.as_view(), name='deck_detail'),
    path('card/<int:pk>/edit', CardEdit.as_view(), name='card_edit'),
]
urlpatterns += router.urls
