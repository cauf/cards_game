from django.db.models.signals import post_save
from django.dispatch import receiver
from django.contrib.auth.models import Group, User
from random import randint
from game.models import (
    Gamer,
    Deck,
    CardType,
    Card,
)
from game.utils import get_random_name
from cards_game.settings import (
    DEFAULT_BALANCE,
    DECK_SETTINGS,
)


@receiver(post_save, sender=User)
def create_gamer(sender, instance, created, **kwargs):
    if created:
        groups = list(Group.objects.filter(name='Игроки'))
        if len(groups) > 0:
            group = groups[0]
            instance.groups.add(group)
            instance.save()
        dck = Deck(
            display_name=f'Колода игрока {instance.username}',
        )
        dck.save()
        gmr = Gamer(
            user=instance,
            balance=DEFAULT_BALANCE,
            deck=dck,
        )
        gmr.save()


@receiver(post_save, sender=Deck)
def fill_deck(sender, instance, created, **kwargs):
    if created:
        max_count = DECK_SETTINGS['max_count']
        mcfct = DECK_SETTINGS['min_count_for_card_type']
        card_types = list(CardType.objects.filter(is_active=True))
        card_types_count = {ct:0 for ct in card_types}
        for i, card_type in enumerate(card_types):
            if i == len(card_types) - 1:
                card_types_count[card_type] = max_count - \
                    sum(card_types_count.values())
            else:
                max_rand = max_count - sum([
                    mcfct if card_types_count[ct] == 0 else card_types_count[ct]
                    for ct in card_types
                    if ct != card_type
                ])
                card_types_count[card_type] = randint(mcfct, max_rand)
            for _ in range(card_types_count[card_type]):
                card = Card(
                    display_name=get_random_name().capitalize(),
                    strength=randint(10,100),
                    card_type=card_type,
                    deck=instance,
                )
                card.save()
