from django.test import TestCase
from django.contrib.auth.models import User, UserManager
from game.models import (
    Gamer,
    Deck,
    CardType,
    Card,
    TradePosition,
)

class CreateUserCase(TestCase):
    fixtures = ['start_data.yaml']
    username = 'testuser'
    password = 'pswd_9876'
    user = None
    gamer = None
    deck = None
    cards = None

    def _get_gamer(self) -> None:
        gamers = Gamer.objects.filter(user=self.user)
        if len(gamers) == 1:
            self.gamer = gamers[0]
    
    def _get_deck(self) -> None:
        if self.gamer:
            decks = Deck.objects.filter(gamer=self.gamer)
            if len(decks) == 1:
                self.deck = decks[0]
    
    def _get_cards(self) -> None:
        if self.deck:
            cards = Card.objects.filter(deck=self.deck)
            if len(cards) > 0:
                self.cards = cards

    def setUp(self) -> None:
        users = User.objects.filter(username=self.username)
        if len(users) == 0:
            self.user = User(
                username=self.username,
            )
            self.user.set_password(self.password)
            self.user.save()
        else:
            self.user = users[0]
        self._get_gamer()
        self._get_deck()
        self._get_cards()
    
    def test_create_gamer(self) -> None:
        self.assertEqual(False, self.gamer is None)

    def test_create_deck(self) -> None:
        self.assertEqual(False, self.deck is None)

    def test_create_cards(self) -> None:
        self.assertEqual(False, self.cards is None)
    
    def test_count_cards(self) -> None:
        self.assertEqual(len(self.cards), 20)
