from django.forms import (
    ModelForm,
)
from game.models import Card


class CardForm(ModelForm):
    class Meta:
        model = Card
        fields = ['display_name']
