from django.contrib import admin
from game.models import (
    CardType,
    Card,
    Deck,
    Gamer,
    TradePosition,
)


@admin.register(CardType)
class CardTypeAdmin(admin.ModelAdmin):
    pass


class TradePositionTabular(admin.TabularInline):
    model = TradePosition
    fk_name = 'seller'
    # fields = '__all__'


class CardTabular(admin.TabularInline):
    model = Card
    fields = ['card_type', 'display_name', 'strength']


@admin.register(Deck)
class DeckAdmin(admin.ModelAdmin):
    model = Deck
    fields = ['display_name']
    inlines = (CardTabular,)


@admin.register(Gamer)
class GamerAdmin(admin.ModelAdmin):
    inlines = (TradePositionTabular,)


@admin.register(TradePosition)
class TradePositionAdmin(admin.ModelAdmin):
    pass
