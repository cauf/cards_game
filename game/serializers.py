from rest_framework import fields, serializers
from game.models import (
    Gamer,
    CardType,
    Card,
    Deck,
    TradePosition,
)
from django.contrib.auth.models import User


class GamerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Gamer
        fields = '__all__'


class CardTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = CardType
        exclude = ('is_active', )


class CardSerializer(serializers.ModelSerializer):
    class Meta:
        model = Card
        fields = '__all__'


class DeckSerializer(serializers.ModelSerializer):
    class Meta:
        model = Deck
        fields = '__all__'


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'username')


class CustGamerSerializer(serializers.Serializer):
    id = serializers.IntegerField()
    user = UserSerializer(read_only=True)
    deck = DeckSerializer(read_only=True)

    class Meta:
        model = Gamer

