from django.db import models
from django.contrib.auth.models import User


TRADE_STATUSES = (
    ('posted', 'размещено'),
    ('canceled', 'отменено'),
    ('completed', 'завершено'),
)


class CardType(models.Model):
    type_name = models.CharField(
        'название',
        max_length=30,
        null=False,
        default='',
    )
    is_active = models.BooleanField('активен', default=True)

    class Meta:
        verbose_name = 'тип карты'
        verbose_name_plural = 'типы карт'

    def __str__(self) -> str:
        return self.type_name

    def __repr__(self) -> str:
        return f'<CardType "{self.type_name}">'


class Card(models.Model):
    display_name = models.CharField(
        'название',
        max_length=30,
        null=False,
        default='',
    )
    strength = models.SmallIntegerField('сила')
    card_type = models.ForeignKey(
        CardType,
        on_delete=models.CASCADE,
        verbose_name='тип карты',
        # editable=False,
    )
    deck = models.ForeignKey(
        'Deck',
        on_delete=models.CASCADE,
        verbose_name='колода',
    )

    class Meta:
        verbose_name = 'карта'
        verbose_name_plural = 'карты'

    def __str__(self) -> str:
        return f'{self.card_type.type_name.capitalize()} ' + \
            f'{self.display_name.capitalize()}'

    def __repr__(self) -> str:
        return f'<Card "{self.display_name}">'


class Deck(models.Model):
    display_name = models.CharField(
        'название',
        max_length=30,
        null=False,
        default='',
    )

    class Meta:
        verbose_name = 'колода'
        verbose_name_plural = 'колоды'

    def __str__(self) -> str:
        return self.display_name

    def __repr__(self) -> str:
        return f'<Deck "{self.display_name}">'


class Gamer(models.Model):
    user = models.OneToOneField(
        User,
        on_delete=models.DO_NOTHING,
        verbose_name='пользователь',
    )
    deck = models.OneToOneField(
        Deck,
        on_delete=models.CASCADE,
        verbose_name='колода',
    )
    balance = models.DecimalField(
        'баланс',
        max_digits=10,
        decimal_places=2,
    )

    class Meta:
        verbose_name = 'игрок'
        verbose_name_plural = 'игроки'

    def __str__(self) -> str:
        return self.user.username

    def __repr__(self) -> str:
        return f'<Gamer "{self.user.username}">'


class TradePosition(models.Model):
    card = models.ForeignKey(
        Card,
        on_delete=models.SET_NULL,
        verbose_name='карта',
        null=True,
    )
    price = models.DecimalField(
        'цена',
        max_digits=10,
        decimal_places=2,
    )
    status = models.CharField(
        'статус',
        max_length=10,
        choices=TRADE_STATUSES,
    )
    seller = models.ForeignKey(
        Gamer,
        on_delete=models.SET_NULL,
        verbose_name='продавец',
        related_name='seller',
        null=True,
    )
    buyer = models.ForeignKey(
        Gamer,
        on_delete=models.SET_NULL,
        verbose_name='покупатель',
        related_name='buyer',
        null=True,
        blank=True,
    )
    create_date = models.DateTimeField(
        'дата создания',
        auto_now=True,
    )
    deal_date = models.DateTimeField(
        'дата сделки',
        blank=True,
    )

    class Meta:
        verbose_name = 'торговая позиция'
        verbose_name_plural = 'торговые позиции'

    def __str__(self) -> str:
        return f'{self.seller.user.username} - {self.card.__str__()}'

    def __repr__(self) -> str:
        return f'<TradePosition seller:"{self.seller.user.username}" ' + \
            f'card:"{self.card.__str__()}"'
